<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2020-2022 Harald Sitter <sitter@kde.org>
-->

# Setup

- `pipenv install`
- install redis
- configure redis to provide a unix socket
- make a config (examples/config.ini)
- start server.py to provide the webhook interface
- start worker.py to provide the worker for processing events

# How it works

server.py gets hook events from gitlab -> puts events into a
redis list -> worker triggers on list items and processes them

# Test

- `pipenv run pytest`
