# SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# vim: tabstop=8 softtabstop=0 noexpandtab shiftwidth=8
from .WebResources import GitlabSystemHookResource
