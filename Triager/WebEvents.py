# vim: tabstop=8 softtabstop=0 noexpandtab shiftwidth=8
#
# SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
# SPDX-FileCopyrightText: 2020-2022 Harald Sitter <sitter@kde.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import bugzilla
import gitlab
import Triager
import json
import re
import sys
from collections import defaultdict

# This isn't the cleanest way to do things, but we don't have another way of passing state around unfortunately
gitlabServer = None
bugzillaServer = None

hookRes = {
	'bug_fixed': re.compile("^\s*(?:BUGS?|FEATURE)[:=]\s*(.+)"),
	'bug_cc': re.compile("^\s*CCBUGS?[:=]\s*(.+)")
}

# Updates a given merge request as requested
def updateMergeRequest( gitlabProject , merge_request_id ):
	# Get the merge request
	gitlabMergeRequest = gitlabProject.mergerequests.get(merge_request_id)

	# Now we set allow_collaboration flag on the merge request
	gitlabMergeRequest.allow_collaboration = True

	# Now we set title to same title it was, this is hack to make gitlab accept our API request
	# todo: refactor this out
	gitlabMergeRequest.title = gitlabMergeRequest.title

	# Save the merge request
	gitlabMergeRequest.save()

	# All done!
	return True

def assignBugzilla(event):
	description = event['object_attributes']['description']
	url = event['object_attributes']['url']

	results = defaultdict(list)
	for line in description.split("\n"):
		for (name, regex) in hookRes.items():
			match = re.match(regex, line)
			if match:
				results[name] += re.findall("(\d{1,10})", match.group(1))

	commentBody = 'A possibly relevant merge request was started @ %s' %url

	for fixed in results['bug_fixed']:
		# states defined at https://bugstest.kde.org/rest/field/bug/8
		openStates = ['UNCONFIRMED', 'CONFIRMED', 'REOPENED', 'NEEDSINFO', 'ASSIGNED']
		bug = bugzillaServer.get_bug(fixed)
		if not bug.status in openStates:
			print("Bug %s not in an open state %s" % (fixed, bug.status))
			continue

		print('Assigning bug %s' % fixed)
		update = bugzilla.DotDict()
		update.status = 'ASSIGNED'
		update.comment = bugzilla.DotDict()
		update.comment.body = commentBody
		bugzillaServer.put_bug(fixed, update)

	for cced in results['bug_cc']:
		print('CCing bug %s' % cced)
		bugzillaServer.post_comment(cced, commentBody)

def process(body):
	event = json.loads(body.decode('utf-8'))

	if event.get('event_type', '') == 'merge_request':
		print("merge request!")
		project = gitlabServer.projects.get(event['project']['id'])
		if project.namespace['kind'] != 'group':
			print("Target project not a group -> not fiddling with the MR!")
			return

		if event['object_attributes']['action'] == 'open':
			# TODO: this would be loads better if we sent exceptions to a
			#   sentry. otherwise we'll not know when things explode
			try:
				updateMergeRequest(project, event['object_attributes']['iid'])
			except Exception as e:
				print("Error: ", e)

			try:
				assignBugzilla(event)
			except Exception as e:
				print("Error: ", e)
