# vim: tabstop=8 softtabstop=0 noexpandtab shiftwidth=8

# SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
# SPDX-FileCopyrightText: 2020 Ben Cooksley <bcooksley@kde.org>
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import falcon

# Falcon Resource handler for Gitlab System Hook events
class GitlabSystemHookResource(object):

	# Initial Setup
	def __init__(self, redisQueue, gitlabToken):
		# Store everything we need to operate
		self.redisQueue  = redisQueue
		self.gitlabToken = gitlabToken

	# Receive a submission from Gitlab
	def on_post(self, req, resp):
		# First check to make sure the submission comes from Gitlab
		if req.get_header('X-Gitlab-Token') != self.gitlabToken:
			raise falcon.HTTPForbidden('Your request is not valid.')

		# Now that we know we are dealing with a Gitlab System Hook submission, we can decode it
		# We expect to receive a JSON formatted payload
		try:
			body = req.stream.read(req.content_length or 0)
		except:
			raise falcon.HTTPBadRequest('Invalid Submission', 'Valid JSON documents are required.')

		# Queue for processing in worker.
		self.redisQueue.enqueue('Triager.WebEvents.process', body)

		# Give Gitlab the all clear that we've done what is needed
		resp.status = falcon.HTTP_200
		resp.body = json.dumps({'message': 'OK'})
