#!/usr/bin/env python

# SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import bugzilla
import os
import rq
import sys
import redis
import gitlab
import argparse
import Triager
import configparser
import Triager.WebEvents

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Webservice worker to receive notifications from Gitlab and queue them for processing')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
	print("Unable to locate specified configuration file: %s".format(args.config))
	sys.exit(1)

# Read in our configuration
configuration = configparser.ConfigParser( interpolation=configparser.ExtendedInterpolation() )
configuration.read( args.config, encoding='utf-8' )

# Connect to the upstream Gitlab server we will be working with
# To do this, we need to get our credentials and hostname first
gitlabHost  = configuration.get('Gitlab', 'instance')
gitlabToken = configuration.get('Gitlab', 'token')
# Now we can actually connect
gitlabServer = gitlab.Gitlab( gitlabHost, private_token=gitlabToken )

# Ensure the gitlab server instance is available to workers
Triager.WebEvents.gitlabServer = gitlabServer

bugzillaInstance = configuration.get('Bugzilla', 'instance')
bugzillaAPIKey = configuration.get('Bugzilla', 'api-key')
Triager.WebEvents.bugzillaServer = bugzilla.Bugzilla(url=('%s/rest/' % bugzillaInstance), api_key=bugzillaAPIKey)

# Connect to the Redis Server
redisSocket = configuration.get('Webservice', 'redis-socket')
redisServer = redis.Redis( unix_socket_path=redisSocket )

# Determine the name of the queue we'll be processing...
queueName   = configuration.get('Webservice', 'queue-name')

# Start processing our work...
worker = rq.Worker( queues=[queueName], connection=redisServer )
worker.work()

# vim: tabstop=8 softtabstop=0 noexpandtab shiftwidth=8
