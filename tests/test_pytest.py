# SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

from Triager import WebEvents

from unittest.mock import Mock
import json


def test_ccbug(shared_datadir):
    body = (shared_datadir / 'mr-with-ccbug.json').read_bytes()

    WebEvents.bugzillaServer = Mock()
    WebEvents.assignBugzilla(json.loads(body.decode('utf-8')))

    WebEvents.bugzillaServer.post_comment.assert_called_with('123', 'A possibly relevant merge request was started @ https://invent.kde.org/sitter/demo/-/merge_requests/9')


def test_bug(shared_datadir):
    body = (shared_datadir / 'mr-with-bug.json').read_bytes()

    WebEvents.bugzillaServer = Mock()
    bug = Mock()
    WebEvents.bugzillaServer.get_bug.return_value = bug
    bug.status = 'UNCONFIRMED'
    WebEvents.assignBugzilla(json.loads(body.decode('utf-8')))

    WebEvents.bugzillaServer.put_bug.assert_called_with('321', {'status': 'ASSIGNED', 'comment': {'body': 'A possibly relevant merge request was started @ https://invent.kde.org/sitter/demo/-/merge_requests/9'}})


def test_unexpected_payload(shared_datadir):
    # Don't go belly up on unknown/unexpected payloads
    body = (shared_datadir / 'unexpected-payload.json').read_bytes()

    WebEvents.process(body)
